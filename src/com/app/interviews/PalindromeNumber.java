package com.app.interviews;

import java.util.Scanner;

public class PalindromeNumber {
	
	static int r,sum=0,temp;
	
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("enter your number");
	int num=sc.nextInt();
	//palindrome 121
	temp=num; 
	while(num>0) {//5>0 true again 0 condition fails
		r=num%10;//55%10=5                        //5%10=5
		sum=(sum*10)+r;//(0*10)+5=5               //(5*10)+5=55
		num=num/10;//55/10=5                      //5/10=0
	}
	if(temp==sum) {
		System.out.println("Palindrome number");
	}else {
		System.out.println("not Palindrome number");
	}
}
}
