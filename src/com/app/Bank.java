package com.app;

public class Bank {

	public  static int currentBalance=1000; 
	
	
	public static void greetCustomer() {
		System.out.println("Hi Bank");
		
	}
	
	public void deposit(int amount) {
		currentBalance=currentBalance+amount;
		System.out.println("amount deposited successfully");
	}
	
	public static void withdraw(int amount) {
		currentBalance=currentBalance-amount;
		System.out.println("amount withdrawn successfully");

	}
	
	public int getCurrentBalance() {
		System.out.println("currentBalance="+currentBalance);
		return currentBalance;
	}
	public static void main(String[] args) {
		greetCustomer();
		Bank b=new Bank();
		System.out.println("current balance: "+b.getCurrentBalance());
		b.deposit(5000);
		System.out.println("current balance: "+b.getCurrentBalance());
		withdraw(300);
		System.out.println("current balance: "+b.getCurrentBalance());

	}
}
