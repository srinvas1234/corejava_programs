package com.app.interviews;

import java.util.Scanner;

public class EvenOrOddNumberPrintingExample {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number");
		 int num=sc.nextInt();
		 
		 if(num%2==0) {
			 System.out.println(num+":->It is an Even number");
		 }else {
			 System.out.println(num+":-> It is an Odd number");
		 }
		 
		
	}

}
