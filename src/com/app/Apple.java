package com.app;

public class Apple implements Laptop{
	@Override
	public void copy() {
		System.out.println("Lenova copy code");
	}

	@Override
	public void paste() {
		System.out.println("Lenova paste code");
	}

	@Override
	public void cut() {
		System.out.println("Lenova cut code");
	}

	@Override
	public void keyword() {
		System.out.println("Lenova keyword code");
	}

}
