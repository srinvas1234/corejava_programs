package com.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class TrywithResourcesExample {
public static void main(String[] args) throws Throwable {
	
	File file=new File("./trywithresourcedata/");
	if(!file.exists()) 
		file.createNewFile();
	//autoclosable
	//use :- memory lekage and security issue resolving
	try(FileInputStream fis=new FileInputStream(file);){
	//System.out.println(fis.read());
	
	int asciicode;
	
	try {
		String str=new String();
		while((asciicode=fis.read())!=-1) {
			str+=String.valueOf((char)asciicode);
			//System.out.print((char)asciicode);
		}
		System.out.println(str);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
}
}
}
