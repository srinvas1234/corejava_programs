package com.app;

import java.util.Scanner;

public class SwitchConditionExample1 {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("enter your age");
	int even=sc.nextInt();
	int odd=sc.nextInt();
	switch (even) {
	case 1:
		System.out.println("odd");
		break;
	case 2:
		System.out.println("even");
		break;
	case 3:
		System.out.println("odd");
		break;
	case 4:
		System.out.println("even");
		break;
	default:
		break;
	}
}
}
