package com.app.stringexamples;

import java.util.Arrays;

public class SortedArrayExampleNew {

	public static void main(String[] args) {
		
		String str="welcome";
		
		char[] ch=str.toCharArray();
		
		char temp;
		
		for(int i=0;i<ch.length;i++) {
			for(int j=i+1;j<ch.length;j++) {
				
				if(ch[i]>ch[j]) {
					temp=ch[i];
					ch[i]=ch[j];
					ch[j]=temp;
				}
				
				
			}
		}
		System.out.println(new String(ch));
		
		Arrays.sort(ch);
		System.out.println(new String(ch));
		
		
		
		
		
	}

}
