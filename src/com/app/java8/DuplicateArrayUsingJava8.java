package com.app.java8;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DuplicateArrayUsingJava8 {

	public static void main(String[] args) {

		List<Integer> numbers=Arrays.asList(3,34,1,2,4,3,2,1,3);
		System.out.println("List of Duplicates: "+numbers);
		
		List<Integer> withoutNumbers=numbers.stream().distinct().collect(Collectors.toList());
		System.out.println(withoutNumbers);
		System.out.println("List of remove Duplicates: "+withoutNumbers);
}
}