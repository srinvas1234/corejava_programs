package com.app.stringexamples;

import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicateCharactersExample {

	public static void main(String[] args) {
		
		String str="programming";
		
		//Approach-1
		StringBuilder sb1=new StringBuilder();
		str.chars().distinct().forEach(c->sb1.append((char)c));
		System.out.println(sb1);
		
		//Approach-2
		
		
		StringBuilder sb2=new StringBuilder();
		
		for(int i=0;i<str.length();i++) {
			char ch=str.charAt(i);
			int ide=str.indexOf(ch,i+1);
			if(ide==-1) {
				sb2.append(ch);
			}
		}
		System.out.println(sb2);
		
		//Approch-3
		
		char[] cc=str.toCharArray();
		StringBuilder sb3=new StringBuilder();
		for(int i=0;i<cc.length;i++) {
			
			boolean repeated=false;
			for(int j=i+1;j<cc.length;j++) {
				if(cc[i]==cc[j]) {
					repeated=true;
					break;
				}
			}
			if(!repeated) {
				sb3.append(cc[i]);
			}
		}
		
		System.out.println(sb3);
		
		
		//Approach-4
		StringBuilder sb4=new StringBuilder();
		Set<Character> set=new LinkedHashSet<Character>();
		for(int i=0;i<str.length();i++) {
			set.add(str.charAt(i));
		}
		
		for(Character c:set) {
			sb4.append(c);
		}
		System.out.println(sb4);
		
		
		
	}

}
