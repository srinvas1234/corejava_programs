package com.app.stringexamples;

import java.util.Arrays;

public class CharWithOccuranceExample {

	public static void main(String[] args) {

		String input="opentext";
		char charToReplace='e';
		//expected output=open1ex2
	
		//check char presence in string
	if(input.indexOf(charToReplace)==-1) {
		System.out.println("Given character not present in this string");
		System.exit(0);
	}
	
	
	//Logic to replace char occurance in string
	
	int count=1;
	for(int i=0;i<input.length();i++) {
		char ch=input.charAt(i);
		if(ch==charToReplace) {//t==t>1
			input=input.replaceFirst(String.valueOf(charToReplace), String.valueOf(count));
			count++;
		}
	}
	
	System.out.println(input);
	}
	/*	
		//Logic to replace char occurance in string
		
	char[] ch=input.toCharArray();
	
	int count=0;
	for(int i=0;i<ch.length;i++) {
		if(ch[i]==charToReplace) {//0==t p==t e==t n==t t==t e==t x==t t=t
			ch[i]=String.valueOf(count).charAt(0);
			count++;
		}
	}
	System.out.println(Arrays.toString(ch));
		
	}
*/
}
