package com.app.stringexamples;

import java.util.Arrays;

public class CharWithOccuranceExamplePractice {

	public static void main(String[] args) {
		String input="javanewways";
		char charToReplace='a';
		//expected output=j1v2
		
		if(input.indexOf(charToReplace)==-1) {
			System.out.println("Not present in this string");
			System.exit(0);
		}
		/*
		 * int count=1; char[] ch=input.toCharArray();// j a v a for(int
		 * i=0;i<ch.length;i++) { if(ch[i]==charToReplace) {//j==a a==a v==a a==a
		 * ch[i]=String.valueOf(count).charAt(0); count++; } }
		 * System.out.println(Arrays.toString(ch));
		 */
		int count=1;
		
		for(int i=0;i<input.length();i++) {
		
			char ch=input.charAt(i);
			
			if(ch==charToReplace) {
				input=input.replaceFirst(String.valueOf(charToReplace),String.valueOf(count));
				count++;
			}
			
			
		}
		
		
		
		System.out.println(input);
		
		
		
		
		
		
		
	}

}
