package com.app.java8;

/*
 * 
 *  java.util.function
 * FunctionalInterface- @FunctionalInterface annotation It is not recommended but best practice we can keep this annotation
 * Consumer<T>-void accept(T t);
 *Predicate<T> boolean test(T t)
 * Supplier<T> T get();
 * Function<T, R> -R apply(T t);
 */

@FunctionalInterface
public interface FunctionalInterfaceExample {

	
	public void fun();
	//public void laugh();
	
	
	default void laugh() {
		System.out.println("java 8 default method");
	}
	static void breakN() {
		System.out.println("java 8 static method");
	}
	
	private void bbb() {
		System.out.println("java 9 	private method");
	}
	
}
