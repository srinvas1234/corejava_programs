package com.app;

import java.util.Random;

public class StaticNonstaticExamples {

	static {
		System.out.println("static block");//first execute
	}
	{
		System.out.println("non-static block");
	}
	public StaticNonstaticExamples() {
		System.out.println("constructor");
	}
	 
	static int number;
	public static void main(String[] args) {
		System.out.println("I am good");// 2 execute
		StaticNonstaticExamples s1=new StaticNonstaticExamples(); ///3 execute 
		s1.number=new Random().nextInt();
		
		StaticNonstaticExamples s2=new StaticNonstaticExamples();
		s2.number=new Random().nextInt();
		
		StaticNonstaticExamples s3=new StaticNonstaticExamples();
		s3.number=new Random().nextInt();
		System.out.println(s1.number);
		System.out.println(s2.number);
		System.out.println(s3.number);
		
		System.out.println(StaticNonstaticExamples.number);//using static keyword
		
		
	}

}
