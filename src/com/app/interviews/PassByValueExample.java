package com.app.interviews;

public class PassByValueExample {
	
	int eyes;
	protected String animalName="Mouse";
	public static void changePrimitive(int aCopy) {
		aCopy=13;	
	}
public static void main(String[] args) {
	
	PassByValueExample pp=new PassByValueExample();
	pp.eyes=777;
	
	
	int a=666;
	changePrimitive(a);
	System.out.println("Value of a is+ "+a);
	System.out.println(pp.eyes);
}

}
