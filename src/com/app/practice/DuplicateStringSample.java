package com.app.practice;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class DuplicateStringSample {
	
	public static void DuplicateString(String str) {
		
		LinkedHashMap<String, Integer> lhm=new LinkedHashMap<String, Integer>();
		
		String[] s=str.split(" ");
		for (String tempString : s) {
			
			if(lhm.get(tempString)!=null) {
				lhm.put(tempString, lhm.get(tempString)+1);
			}
			else {
				lhm.put(tempString, 1);
			}
			
		}
		System.out.println(lhm);
		
		Iterator<String> tempString=lhm.keySet().iterator();
		while(tempString.hasNext()) {
			String temp=tempString.next();
			if(lhm.get(temp)>1) {
				System.out.println("the word "+temp+" appeared "+lhm.get(temp)+" no of times");
			}
		}
	}
	
	public static void findDuplicateCharacter(String str) {
		LinkedHashMap<Character, Integer> lhm1=new LinkedHashMap<Character, Integer>();

		for(int i=0;i<str.length();i++) {
		char ch=str.charAt(i);
		if(lhm1.get(ch)!=null) {
			lhm1.put(ch, lhm1.get(ch)+1);
		}else {
			lhm1.put(ch, 1);
		}
			
		}
		
		System.out.println(lhm1);
		
		
	}
	
	
	
	
	
	
	
public static void main(String[] args) {
	
	DuplicateString("I am am good in java and java");
	
	findDuplicateCharacter("welcome");
	
	
	
	
	
	
	
	
	
	
	
	
}
}
