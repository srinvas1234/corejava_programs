package com.app.java8;

public class LamdbaExpression {
public static void main(String[] args) {
		
	Car c1=new Car() {
		
		@Override
		public void drive() {
			System.out.println("AUDI CAR");
			
		}
	}; 	
	c1.drive();
	BMW b=new BMW();
	b.drive();
	
	//Lamdba expresssion with anonymous function
	//3 component: (argument) arrow token(->) function body
Car c2=()->System.out.println("Santro"); 	
	c2.drive();
}
}
class BMW implements Car{
	@Override
	public void drive() {
		System.out.println("BMW CAR");	
	}	
}
@FunctionalInterface
interface Car{
	
	public void drive();
}