package com.app.interviews;

import java.util.Arrays;

public class TwoSumExamplePractice {
	
	public int[] twoSumData(int[] nums,int target) {
		
		for(int i=0;i<nums.length;i++) {
			for(int j=i+1;j<nums.length;j++) {
				if(nums[i]+nums[j]==target) {
					return new int[] {i,j};
				}
			}
		}
		return null;	
	}
public static void main(String[] args) {
	
	int[] nums= {3,4,1,6};
	int target=4;
	TwoSumExamplePractice ss=new TwoSumExamplePractice();
	System.out.println(Arrays.toString(ss.twoSumData(nums,target)));
}
}
