package com.app.interviews;

import java.util.Comparator;

public class ComparableExample implements Comparable<ComparableExample> {

	private int id;
	private String name;
	public ComparableExample() {
		super();
	}
	public ComparableExample(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "ComparableExample [id=" + id + ", name=" + name + "]";
	}
	@Override
	public int compareTo(ComparableExample o) {

		if(id==o.id) {
			return 0;

		}else if (id>o.id) {
			return 1;
		}else {
			return -1;
		}
		
	}
	public static Comparator<ComparableExample> NameComparator=new Comparator<ComparableExample>() {
		
		@Override
		public int compare(ComparableExample o1, ComparableExample o2) {
			return o1.getName().compareTo(o2.getName());
		}
	};

	
	
	
	
}
