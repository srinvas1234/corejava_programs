package com.app;

import java.util.Arrays;

public class String_Examples {

	public static void main(String[] args) {

		String s1="helLo";
		String s2="hello";
		String s3=" Hi Srinivas ";
		String s4="HELLO";
		String s5="Hi how are you ^ I am good";
		String s6="9010";
		String s7;
		String s8="  ";
		String s9="";
		int i=788;
		System.out.println(s1.equalsIgnoreCase(s2));
		System.out.println(s1==s2);
		System.out.println(s1.contains("L"));
		System.out.println(s1.startsWith("he"));
		System.out.println(s1.endsWith("o"));
		System.out.println(s1.length());
		System.out.println(s3.trim());
		System.out.println(s3.trim().length());
		System.out.println(s4.toLowerCase());
		System.out.println(s4.toUpperCase());
		char[] aa=s1.toCharArray();
		System.out.println(aa);
		System.out.println(s2.indexOf("o"));
		System.out.println(s2.lastIndexOf("o"));
		System.out.println(s3.substring(4));
		System.out.println(s1.equals("hello"));
		System.out.println(Arrays.toString(s5.split("\\^")));
		System.out.println(s1.charAt(0));
		System.out.println(s1.replace("e", "u"));
		System.out.println(s2.replace("hello", "how"));
		System.out.println(Integer.valueOf(s6));
		System.out.println(String.valueOf(i));
		System.out.println(s8.length());
		System.out.println(s9.isEmpty());
		
		
		
	
	}

}
