package com.app.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAPIExample {
public static void main(String[] args) {
	
	
	List<Integer> aa=new ArrayList<Integer>();
	aa.add(333);
	aa.add(5);
	aa.add(667);
	aa.add(4);
	//System.out.println(aa);
	
	List<Integer> aa1=new ArrayList<Integer>();
	
	
	/*
	 * aa1=aa.stream().filter(x->x>=15).collect(Collectors.toList());
	 * aa1.stream().forEach(x->System.out.println(x));
	 */
	
	
	
	//aa.stream().filter(i->i%2==0).forEach(x->System.out.println(x));
	//System.out.println(aa1);
	
	
	Stream s=aa.stream().map(i->i*i);
	s.forEach(x->System.out.println(x));
}
}
