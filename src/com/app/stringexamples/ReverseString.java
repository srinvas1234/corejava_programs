package com.app.stringexamples;

public class ReverseString {
	public static void main(String[] args) {
		String str="hello";
		//Approach-1
		System.out.println(str.length());
		char[] ch=str.toCharArray();
		
		for(int i=ch.length-1;i>=0;i--) {
			System.out.print(ch[i]);
		}

		System.out.println();
		//Approach-2
		
		for(int i=str.length()-1;i>=0;i--) {
			System.out.print(str.charAt(i));
		}
		System.out.println();
		//Approach-3
		
		StringBuffer sb=new StringBuffer(str);
		System.out.println(sb.reverse());
		StringBuilder sbb=new StringBuilder(str);
		System.out.println(sbb.reverse());
	
	}

}
