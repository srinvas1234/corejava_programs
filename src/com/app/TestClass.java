package com.app;

/**
 * this class used for constructor
 * Documentation Comments use:- above class ,variables , method...
 * @author srini
 * @since 2023
 * @version java 1.8
 * @category test
 */
public class TestClass {

	public TestClass() {
		System.out.println("default");
	}
	
	public TestClass(int a) {
		System.out.println("paramter");
	}
	/**
	 * 
	 * 
	 * @param this int a
	 */
	public void dd(int a) {
		
	}

	public static void main(String[] args) {
TestClass ss=new TestClass();
TestClass ss1=new TestClass(30);
TestClass ss2=new TestClass(49);
System.out.println(ss.hashCode());
System.out.println(ss1.hashCode());
System.out.println(ss2.hashCode());
ss.dd(333);
	}

}
