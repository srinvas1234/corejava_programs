package com.app.practicenew;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

	public static void main(String[] args) {

		Employee e1=new Employee(122, "srinivas", 78787, Gender.Male);
		Employee e2=new Employee(123, "Nihas", 6787, Gender.Male);
		Employee e3=new Employee(124, "Hihas", 787, Gender.Female);
		Employee e4=new Employee(125, "jihas", 48787, Gender.Female);
		
		List<Employee> empList=new ArrayList<Employee>();
		empList.add(e1);
		empList.add(e2);
		empList.add(e3);
		empList.add(e4);
		
		//I want List if employees who is drawing more than 1000
		
		List<Employee> ss=empList.stream().filter(e->e.getSalary()>10000).collect(Collectors.toList());
		System.out.println(ss);
	}

}
