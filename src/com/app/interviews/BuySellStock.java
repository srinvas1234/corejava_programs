package com.app.interviews;

public class BuySellStock {
	
	public int buySellStock(int[] prices) {
		if(prices.length<2) {
			return 0;
		}
		int minPriceSoFar=prices[0];
		int calculateProfitSoFar=0;
		for(int i=1;i<prices.length;i++) {
			calculateProfitSoFar=Math.max(calculateProfitSoFar, prices[i]-minPriceSoFar);
			minPriceSoFar=Math.min(minPriceSoFar, prices[i]);
		}
		return calculateProfitSoFar;
	}
	
	
	
	
public static void main(String[] args) {
	BuySellStock bb=new BuySellStock();
	int[] prices= {7,1,5,3,6,4};
	System.out.println(bb.buySellStock(prices));
}
}
