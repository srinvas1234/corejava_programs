package com.app.interviews;

public class MethodOverloadingExample {

	
	public void add(int a,int b) {
		System.out.println("I am good: "+a+b);
	}
	public void add(int a,int b,int c) {
		System.out.println("I am bad: "+a+b+c);
	}
	
	public void add(int a,long b) {
		System.out.println("I am ok: "+a+b);
	}
	public void add(long a,int b) {
		System.out.println("I am ok: "+a+b);
	}
	
	/*
	 * protected int add(byte a,short b) { System.out.println("I am ok: "+a+b);
	 * return a+b; }
	 */
}
