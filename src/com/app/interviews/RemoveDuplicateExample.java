package com.app.interviews;

public class RemoveDuplicateExample {

	public static void main(String[] args) {

		int[] a= {4,5,6,3,5,6,7,8};
		
		for(int i=0;i<a.length;i++)
		{
			for(int j=i+1;j<a.length;j++) 
			{
				if(a[i]==a[j])
				{
					a[i]=-1;
				}
			}
		}
		System.out.println("removed array is:");
		for(int i=0;i<a.length;i++) {
			if(a[i]!=-1) 
			{
				System.out.print(a[i]+" ");
			}
		}
		
		
	}

}
