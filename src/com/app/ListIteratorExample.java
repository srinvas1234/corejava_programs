package com.app;

import java.util.ArrayList;
import java.util.ListIterator;

public class ListIteratorExample {
public static void main(String[] args) {
	//ListIterator-Interface
	//Introduced for iterating only list type of collection objects
	//Legacy classes which is version 1.0 
	//Hashtable ,stack,vector,Directory and properties	
	//New classes which is version 1.2
	//since 1.0 >ALL>>Hashtable ,stack,vector,Directory and properties
	//Bi Direction, read-only with special permission for removing elements
	//it was followed by forword and backword directions
	//it will do read and write operations we can add as well and remove
	
	ArrayList<Integer> aaa=new ArrayList<Integer>();
	aaa.add(5676);;
	aaa.add(66);
	aaa.add(666);
	aaa.add(444);
	aaa.add(448);
	System.out.println(aaa);
	ListIterator<Integer> ll=aaa.listIterator();
	
	while(ll.hasNext()) {
		ll.add(7777);
		System.out.println(ll.next());
	}
	System.out.println("**********");
	while(ll.hasPrevious()) {
		System.out.println(ll.previous());
	}
	ll.remove();
	System.out.println(aaa);

	
	
	
	
}
}
