package com.app;

public class Student {

	private int rollNumber;
	private String name;
	private boolean isAttended;
	
	public Student(int rollNumber) {
		
		this.rollNumber=rollNumber;
	}
	
	
	
	public void setStudentAttendance(boolean flag) {
		if(!isAttended)
		isAttended=flag;
		System.out.println("teacher assigned attendance");
	}
	
	public boolean getStudentAttendance() {
		System.out.println("teacher access student data");
		return isAttended;
	}
}
