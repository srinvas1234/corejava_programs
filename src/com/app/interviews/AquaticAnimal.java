package com.app.interviews;

public class AquaticAnimal extends Animal{

	@Override
	public void specialProperty() {
		System.out.println("Lives in water");
	}
}
