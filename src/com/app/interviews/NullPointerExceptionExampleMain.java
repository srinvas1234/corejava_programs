package com.app.interviews;

public class NullPointerExceptionExampleMain {

	public static void main(String[] args) {
		NullPointerExceptionExample ss=null;
		try {
			System.out.println(ss.name);
		} catch (Exception e) {
			 ss=new NullPointerExceptionExample();
			 System.out.println(ss.name);
		}
		
		

	}

}
