package com.app.interviews;

import java.util.Arrays;

public class ComparableExampleArray {

	public static void main(String[] args) {

		ComparableExample[] cc=new ComparableExample[3];
		cc[0]=new ComparableExample(333, "yee");
		cc[1]=new ComparableExample(33, "eeee");
		cc[2]=new ComparableExample(733, "keee");
		/*
		 * Arrays.sort(cc,ComparableExample.NameComparator);
		 * System.out.println("Sorted : "+Arrays.toString(cc));
		 */
		
		 Arrays.sort(cc);
		 System.out.println(Arrays.toString(cc));
	}

}
