package com.app.stringexamples;

import java.util.Arrays;

public class SortedArrayExample {

	public static void main(String[] args) {

		
		String str="rock";
		
		char[] ch=str.toCharArray();
		
		char temp;
		for(int i=0;i<ch.length;i++) {
			for(int j=i+1;j<ch.length;j++) {
				if(ch[i]>ch[j]) {
					temp=ch[i];
					ch[i]=ch[j];
					ch[j]=temp;
				}
			}
		}
		System.out.println(new String(ch));
	
		//Approach-2
		
		String str1="rock";
		char[] chh=str1.toCharArray();
		Arrays.sort(chh);
		System.out.println(new String(chh));
		
	}

}
