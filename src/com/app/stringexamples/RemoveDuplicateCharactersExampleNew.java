package com.app.stringexamples;

import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicateCharactersExampleNew {

	public static void main(String[] args) {
		
		String str="welcome";
		
		
		StringBuilder sb=new StringBuilder();
		str.chars().distinct().forEach(c->sb.append((char)c));
		System.out.println(sb);
		
		
		
		StringBuilder sb1=new StringBuilder();
		
		for(int i=0;i<str.length();i++) {
			char ch=str.charAt(i);
			
			int idx=str.indexOf(ch, i+1);
			
			if(idx==-1) {
				sb1.append(ch);
			}
		}
		System.out.println(sb1);
		
		
		char[] chc=str.toCharArray();
		StringBuilder sb2=new StringBuilder();
		
		for(int i=0;i<chc.length;i++) {
			boolean repeated=false;
			for(int j=i+1;j<chc.length;j++) {
				
				if(chc[i]==chc[j]) {
					repeated=true;
					break;
				}
			}
			if(!repeated) {
				sb2.append(chc[i]);
			}
		}
		System.out.println(sb2);
		
		
		StringBuilder sb3=new StringBuilder();
		Set<Character> set=new LinkedHashSet<Character>();
		
		for(int i=0;i<str.length();i++) {
			set.add(str.charAt(i));
		}
		
		
		for (Character character : set) {
			sb3.append(character);
		}
		System.out.println(sb3);
		

	}

}
