package com.app.interviews;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class DuplicateStringWord {
	
	
	public static void DuplicateStringWord(String str) {
		
		LinkedHashMap<String, Integer> lhm=new LinkedHashMap<String, Integer>();
		String[] s=str.split(" ");
		
		
		for (String tempstring : s) {
		//System.out.println(tempstring);
			
			if(lhm.get(tempstring)!=null) {
				lhm.put(tempstring, lhm.get(tempstring)+1);
			}
			else {
				lhm.put(tempstring, 1);
			}
			
		}
		System.out.println(lhm);
		Iterator<String> tempstring=lhm.keySet().iterator();
		while(tempstring.hasNext()) {
			String temp=tempstring.next();
			if(lhm.get(temp)>1) {
				System.out.println("the word "+temp+" appeared "+lhm.get(temp)+" no of times");
			}
		}
	
		
		
	}
	
	
	
	public static void findDuplicateCharacter(String str) {
		LinkedHashMap<Character, Integer> lmm=new LinkedHashMap<Character, Integer>();
		
		for(int i=0;i<str.length();i++) {
			char ch=str.charAt(i);
			
			
			if(lmm.get(ch)!=null){
				lmm.put(ch, lmm.get(ch)+1);
				}else {
					lmm.put(ch, 1);
				}
			}
System.out.println(lmm);
	}
	
	
	
	
	
	
	

	public static void main(String[] args) {
		
		
		
		DuplicateStringWord("I am good to start good to end");
		
		findDuplicateCharacter("javaj2ee");

	}

}
