package com.app.stringexamples;

public class CharWithOccuranceExampleNew {

	public static void main(String[] args) {
		
		String str="welcome";
		char charToReplace='e';
		
		if(str.indexOf(charToReplace)==-1)
		{
			System.out.println("Given character not available");
			System.exit(0);
		}

int count=1;
		for(int i=0;i<str.length();i++) {
			char ch=str.charAt(i);
			if(ch==charToReplace) {
				str=str.replaceFirst(String.valueOf(charToReplace),String.valueOf(count));
				count++;
			}
		}
		
		System.out.println(str);
	}

}
