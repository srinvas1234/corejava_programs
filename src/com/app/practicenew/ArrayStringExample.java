package com.app.practicenew;

public class ArrayStringExample {

	public static void main(String[] args) {

		String[] my_array= {"abc","bcd","abc","ccc","ddd","ccc"};
	
		//no of elements is length
		/*
		 * System.out.println(my_array[0]); System.out.println(my_array[1]);
		 * System.out.println(my_array[2]); System.out.println(my_array[3]);
		 * System.out.println(my_array[4]); System.out.println(my_array[5]);
		 * System.out.println(my_array.length-1);//5
		 * System.out.println(my_array.length);//6
		 */	
		for(int i=0;i<my_array.length-1;i++) {//0<5
			for(int j=0;j<my_array.length;j++) {//0<6
				//0.equals0 && 0!=0
				if(my_array[i].equals(my_array[j]) && i!=j){
					System.out.println("Duplicate Array element: "+my_array[j]);
				}
			}
		}
		
	}

}
