package com.app.stringexamples;

public class ReverseStringExample {

	public static void main(String[] args) {
		
		String str="welcome";
		System.out.println(str.length());
		
		for(int i=str.length()-1;i>=0;i--) {
			System.out.print(str.charAt(i));
		}
		System.out.println();
		 char[] ch=str.toCharArray();
		for(int i=ch.length-1;i>=0;i--) {
			System.out.print(ch[i]);
		}
		System.out.println();
		 StringBuilder sb=new StringBuilder(str);
		 System.out.println(sb.reverse());
		 
		 StringBuffer sbb=new StringBuffer(str);
System.out.println(sbb.reverse());
	}

}
