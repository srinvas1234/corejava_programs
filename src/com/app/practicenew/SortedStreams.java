package com.app.practicenew;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortedStreams {

	public static void main(String[] args) {

		System.out.println("below for sorted");
		List<String> list=Arrays.asList("5","r","4","d","t","7","f");
		List<String> sortedList=list.stream().sorted().collect(Collectors.toList());
		sortedList.forEach(System.out::print);
		
		System.out.println("below for reverse sorted");
		
		List<String> sortedList1=list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		sortedList1.forEach(System.out::println);
		
	}

}
