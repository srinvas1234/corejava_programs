package com.app.interviews;

import java.util.Scanner;

public class FactorialExample {

	
	static int factorialExample(int num) {
		if(num==0) {
			return 1;
		}
		else {
			return(num*factorialExample(num-1));
		}
	}
	
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter First Number");
		int num=sc.nextInt();
		int fact=1;
		fact=factorialExample(num);
		System.out.println("Factorial of num "+num+" is :"+fact);
	}

}
