package com.app;

import java.util.Date;

public class PrintStatementsExamples{
public static void main(String[] args) {
	System.out.println("Hello srinivas");
	System.out.println("How are you");
	System.err.printf("the current year %d and month is  %s",2023,"March");
	System.err.printf("The DATE is %Tc\n", new Date() );
	System.err.println("Good");
}
}
