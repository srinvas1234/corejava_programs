package com.app;


public class POJOExample{

	//POJO IS non Serializable
	
	private int id;
	private String name;
	private double salary;
	
	
	public POJOExample() {
		System.out.println("defalut constructor");
	}
	/*
	 * public POJOExample(int id,String name,double salary) { this.id=id;
	 * this.name=name; this.salary=salary;
	 * System.out.println("this is parameter constructor"); }
	 */
	
	public void setId(int id) {
		this.id=id;
	}
	public int getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	public String getName() {
		return name;
		
	}
	
	public void setSalary(double salary)
	{
		this.salary=salary;
	}
	public double getSalary() {
		return salary;
	}

	@Override
	public String toString() {
		return "POJOExample [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	
	
	
}
