package com.app.interviews;

import java.util.ArrayList;
import java.util.Collections;

public class ComparableExampleMain {

	public static void main(String[] args) {
		/*
		 * ComparableExample cc=new ComparableExample(); cc.setId(666);
		 * cc.setName("Srinivas"); System.out.println(cc); ComparableExample cc1=new
		 * ComparableExample(); cc1.setId(566); cc1.setName("Nihas");
		 * System.out.println(cc1); ComparableExample cc2=new ComparableExample();
		 * cc2.setId(766); cc2.setName("Raj");
		 * 
		 * System.out.println(cc2); Collections.sort(null);
		 */
		
		
		ArrayList<ComparableExample> all=new ArrayList<ComparableExample>();
		all.add(new ComparableExample(333, "srinivas"));
		all.add(new ComparableExample(433, "nihas"));
		all.add(new ComparableExample(633, "raj"));
		all.add(new ComparableExample(233, "ramesh"));
		System.out.println(all);
		Collections.sort(all,ComparableExample.NameComparator);
		
		for (ComparableExample comparableExample1 : all) {
		System.out.println(comparableExample1);	
		}
		
		
		
	}

}
