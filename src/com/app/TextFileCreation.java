package com.app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class TextFileCreation {
	public static void main(String[] args) throws IOException {

		File file = new File("./NewFileCreation.txt");
/*
		if (!file.exists())
			file.createNewFile();

		file.delete();
		file.createNewFile();*/
		/*
		 * FileInputStream fis=new FileInputStream(file);
		 * System.out.println((char)fis.read());
		 */
		/*
		 * FileInputStream fis=new FileInputStream(file); int asciicode; String text=new
		 * String(); while ((asciicode=fis.read())!=-1) { text
		 * +=String.valueOf((char)asciicode); System.out.print((char)asciicode);
		 * 
		 * } System.out.println(); System.out.println(text);
		 */
		// fis.close();

		// Scanner sc=new Scanner(file);>>>first way
		/*
		 * Scanner sc=new Scanner(new FileInputStream(file));//second way
		 * 
		 * String text=new String(); while(sc.hasNextLine()) { text
		 * +=sc.nextLine()+"\n"; //System.out.println(sc.nextLine()); }
		 * System.out.println(text); sc.close();
		 */

		/*
		 * FileReader fr=new FileReader(file); int asciicode; String text=new String();
		 * while((asciicode=fr.read())!=-1) { text +=String.valueOf((char)asciicode);;
		 * System.out.print((char)asciicode); } System.out.println();
		 * System.out.println(text);
		 * 
		 * fr.close();
		 */

		/*
		 * FileReader fr=new FileReader(file); FileInputStream fis=new
		 * FileInputStream(file); InputStreamReader isr=new InputStreamReader(fis);
		 * BufferedReader br=new BufferedReader(fr);
		 * 
		 * int asciicode; String text=new String(); while((asciicode=br.read())!=-1)
		 * text +=String.valueOf((char)asciicode); //System.out.print((char)asciicode);
		 * 
		 * //System.out.println(); System.out.println(text);
		 * 
		 */

		// actual logic
		/*
		 * String line = new String(); String text = new String(); while ((line =
		 * br.readLine()) != null) { text += line + "\n"; System.out.println(line); }
		 * System.out.println(text);
		 * 
		 * fis.close(); isr.close(); br.close(); fr.close();
		 */
		/*
		 * String str="hello"; FileOutputStream fos=new FileOutputStream(file); for(char
		 * ch:str.toCharArray()) { fos.write((int)ch); }
		 * 
		 * fos.write(72); fos.write(69); fos.write(76); fos.write(76); fos.write(79);
		 * 
		 * String str="hello world"; FileWriter fw=new FileWriter(file);
		 * //fw.write(str); fw.write(str.toCharArray());
		 */

		
		
		
		
		//FileWriter fw=new FileWriter(file);
		//BufferedWriter bw=new BufferedWriter(fw);
		String existingText=new String();
		String line=new String();
		BufferedReader br=new BufferedReader(new FileReader(file));
		while((line=br.readLine())!=null)
			existingText +=line+"\n";
	
		String str="how are you what are you doing";
		BufferedWriter bw=new BufferedWriter(new FileWriter(file));
		bw.write( existingText+ str);
		//bw.write(str.toCharArray());
		bw.flush();
		bw.close();// >>>internally we use flush method we can keep both no problem at all

	}	}


