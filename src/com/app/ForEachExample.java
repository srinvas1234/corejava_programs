package com.app;

public class ForEachExample {

	public static void main(String[] args) {

		int[] arr= {2,34,53,123,556,654,33,4,5};
		/*
		 * for (int i = 0; i < arr.length; i++) { System.out.println(arr[i]); }
		 */
		
		for (int i : arr) {
			System.out.println(i+"");
		}
	}

}
