package com.app.stringexamples;

import java.util.ArrayList;
import java.util.Arrays;

public class SortedArrayExamplePractice {
public static void main(String[] args) {
	
	
	String str="bcsjkdflmvn";
	
	char temp;
	char[] ch=str.toCharArray();
	for(int i=0;i<ch.length;i++) {
		for(int j=i+1;j<ch.length;j++) {
			if(ch[i]>ch[j]) {
			 temp=ch[i];
			 ch[i]=ch[j];
			 ch[j]=temp;
			}
		}
	}
	System.out.println(new String(ch));
	
	String str1="biscuit";
	
	char[] ccc=str1.toCharArray();
	
	Arrays.sort(ccc);
	
	
	System.out.println(new String(ccc));
	
	
	
	
	
}
}
