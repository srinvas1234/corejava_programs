package com.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class IteratorExample {

public static void main(String[] args) {
	
	//Iterator-Interface
	//Introduced for iterating the legacy collection objects
	//Legacy classes which is version 1.0 
	//Hashtable ,stack,vector,Directory and properties	
	//New classes which is version 1.2
	//since 1.0 >ALL>>Hashtable ,stack,vector,Directory and properties
	//One Direction, read-only with special permission for removing elements
	//forword direction it is not applicable via map after retrive it will applcable
	//applicable for remove operation
	ArrayList<Integer> al=new ArrayList<Integer>();
	al.add(77);
	al.add(676);
	al.add(6767);
	al.add(4545);
	
	for (Integer all : al) {
		System.out.println(all);
	}
	
	
	System.out.println("**********Iterator interface use");
	Iterator<Integer> sss=al.iterator();
	while(sss.hasNext()) {
		System.out.println(sss.next());
		sss.remove();	
		
	}
	System.out.println("data removed: "+sss.toString());
	System.out.println(al);
	
	
	HashMap<String, Integer> age=new HashMap<String, Integer>();
	age.put("srinivas", 28);
	age.put("Nihas", 18);
	age.put("sneha", 26);
	age.put("siddu", 23);
	age.keySet().iterator();
	age.values().iterator();
	age.entrySet().iterator();
	System.out.println(age);
	
	
	
	
	
	
	
	
}}
