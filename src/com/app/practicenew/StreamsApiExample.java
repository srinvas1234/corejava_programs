package com.app.practicenew;

import java.util.Comparator;
import java.util.stream.Stream;

public class StreamsApiExample {

	public static void main(String[] args) {

		// Get Min or Max Number
		Integer maxNumber = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
		          .max(Comparator.comparing(Integer::valueOf))
		          .get();
		 
		Integer minNumber = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
		          .min(Comparator.comparing(Integer::valueOf))
		          .get();
		
		System.out.println(minNumber);
		System.out.println(maxNumber);
		System.out.println("*************below I am practicing integer");
		
		Integer min=Stream.of(6,5,6,3,511,3333,23,23,26).min(Comparator.comparing(Integer::valueOf)).get();
		Integer max=Stream.of(6,5,6,3,511,3333,23,23,26).max(Comparator.comparing(Integer::valueOf)).get();
		System.out.println(min);
		System.out.println(max);
		
		
System.out.println("*************below I am practicing String");
		
		String min1=Stream.of("a","b","r","d","s","v","q","x").min(Comparator.comparing(String::valueOf)).get();
		String max1=Stream.of("a","b","r","d","s","v","q","x").max(Comparator.comparing(String::valueOf)).get();
		System.out.println(min1);
		System.out.println(max1);
	}

}
