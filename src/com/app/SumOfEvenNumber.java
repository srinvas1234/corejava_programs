package com.app;

import java.util.Scanner;

public class SumOfEvenNumber {

	public static void main(String[] args) {
		 
        Scanner console = new Scanner(System.in) ;
        System.out.println("Enter Start Number") ;
        int start =console.nextInt();
        System.out.println("Enter End Number") ;
        int end =console.nextInt();
        
        int sum = 0;
        System.out.println("The even numbers between "+start+" and "+end+" are the following: ");
        for (int r = start; r <= end; r++)
        {
        //if number%2 == 0 it means its an even number
        if(r % 2 == 0)
        {
            sum += r;
        System.out.println(r);
        }
        }
        System.out.println("The sum of even numbers between :"+start +" - "+end +" is : "+sum);
    }
}