package com.app.interviews;

public class VisibiltyAccessChildClass extends VisibiltyAccessParentClass {

	//Cannot reduce the visibility of the inherited method from VisibiltyAccessParentClass
	//dont keep private
	//dont keep protected
	 public void visibility() {
		System.out.println("visibility for child");
	}
	
	
}
