package com.app.interviews;

public class MethodHidingExample {

	
	public static void method1() {
		System.out.println("Method-1 of the MethodHidingExample class is executed.");  
		}
	
	
	public void method2() {
		System.out.println("Method-2 of the MethodHidingExample class is executed.");  
	}
}
