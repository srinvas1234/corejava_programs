package com.app.practicenew;

import java.util.Arrays;
import java.util.Collections;

public class StringArraySort {
public static void main(String[] args) {
	
	String[] str= {"you","me","I","he","she","it"};
	
	//without predefined
	
	for(int i=0;i<str.length-1;i++) {
		for(int j=i+1;j<str.length;j++) {
			if(str[i].compareTo(str[j])>0) {
				String temp=str[i];
				str[i]=str[j];
				str[j]=temp;
			}
		}
	}
	System.out.println(Arrays.toString(str));
	
	System.out.println("***********");
	Arrays.sort(str);
	System.out.println(Arrays.toString(str));

System.out.println("*******");

System.out.println("***********");
Arrays.sort(str,Collections.reverseOrder());
System.out.println(Arrays.toString(str));

}

//predefine




}
