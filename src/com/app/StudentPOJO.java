package com.app;

import java.io.Serializable;

public class StudentPOJO implements Serializable{

	//example we have two layers one for application layer another for db layer 
	//in this case data will go when Serialize the object from application layer to db layer based on Serializer object
	/**
	 * @author srini
	 * @serial
	 */
	private static final long serialVersionUID = 1L;//SerialVersionUID is a unique identifier for each class,
	private int stId;
	private String name;
	public StudentPOJO() {
		super();
	}
	/**
	 * @author srini
	 * @param studentPojo
	 */
	public StudentPOJO(int stId, String name) {
		super();
		this.stId = stId;
		this.name = name;
	}
	public int getStId() {
		return stId;
	}
	public void setStId(int stId) {
		this.stId = stId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "StudentPOJO [stId=" + stId + ", name=" + name + "]";
	}
	
	
}
