package com.app.practicenew;

//public class
public class Singleton {

	//private static variable
	private static Singleton single_instance=null;
	
	////public variable
	public String s;
	
	//private constructor
	private Singleton() {
		s="I am good";
	}
	
	//static instance
	public static synchronized  Singleton getInstance() {
		if(single_instance==null) {
			single_instance=new Singleton();
		}
		return single_instance;
	}
	
	
	
	public static void main(String[] args) {

		Singleton x=Singleton.getInstance();
		System.out.println(x.hashCode());
		Singleton y=Singleton.getInstance();
		System.out.println(y.hashCode());
		Singleton z=Singleton.getInstance();
		System.out.println(z.hashCode());
		
		if(x==y && y==z) {
			System.out.println("same memory pointing");
		}else {
			System.out.println("different memory pointing");
		}
	}

}
