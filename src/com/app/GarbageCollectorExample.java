package com.app;

public class GarbageCollectorExample {
	
	
	@Override
	public void finalize() {
		//The method finalize() from the type Object is deprecated since version 9
		System.out.println("Finalize method called");
	}
	
	
public static void main(String[] args) {
	//below garbage collection 
	//System.gc();
	//Runtime.getRuntime().gc();
	
	//Unused objects
	//un-referenced objects
	//anonymous objects
	
	
	GarbageCollectorExample gg=new GarbageCollectorExample();
	GarbageCollectorExample gg1=new GarbageCollectorExample();
	GarbageCollectorExample gg2=new GarbageCollectorExample();

	gg.finalize();
	new GarbageCollectorExample();//annonymous object
	gg=null;//un referenced object
	//System.out.println(gg);
	gg1=gg2;
System.gc();
}
}
