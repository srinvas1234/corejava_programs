package com.app.interviews;

import java.util.Arrays;

public class TwoSumExample {
	public int[] twoSum(int[] nums,int target) {
		for(int i=0;i<nums.length;i++) {
			for(int j=i+1;j<nums.length;j++) {
				if(nums[i]+nums[j]==target) {
					return new int[] {i,j};
				}
			}
		}
		return null;	
	}
	public static void main(String[] args) {	
		int[] nums= {2,7,11,15};
		int target=17;
		TwoSumExample ss=new TwoSumExample();
		System.out.println(Arrays.toString(ss.twoSum(nums, target)));
	}

}
