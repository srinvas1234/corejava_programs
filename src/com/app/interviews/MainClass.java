package com.app.interviews;

public class MainClass {
public static void main(String[] args) {
	//System.out.println("parent ");
	Animal aa1=new Animal();
	aa1.specialProperty();
	//System.out.println("parent to child");
	Animal aa=new AquaticAnimal();
	aa.specialProperty();
	//System.out.println("child to child");
	System.out.println("******");
	AquaticAnimal s=new AquaticAnimal();
	s.specialProperty();
	
}
}
