package com.app;

public class LoopingStatements {
public static void main(String[] args) {
	
	int i=0;
	/*
	 * while (i<10) { //statements System.out.println(i); i++; }
	 */
	
	do {
		//statements
		System.out.println(i);
		i++;
		} 
	while (i<10);
	
	
	
}
}
