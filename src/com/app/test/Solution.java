package com.app.test;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		 int r;
	        double pi = 3.14, area;
	        Scanner s = new Scanner(System.in);
	        System.out.print("Enter radius of circle: ");
	        r = s.nextInt();
	        area=3.14*(r*r);
	        System.out.println("Area of circle: "+area);
	    }         

}
