package com.app;

import java.util.Scanner;

public class ForExamplePrimeNumbers {
public static void main(String[] args) {
 int num,min,max,i,count;
Scanner scanner=new Scanner(System.in);
System.out.print("Please enter the minimum value: ");
min=scanner.nextInt();
System.out.print("Please enter the maximum value: ");
max=scanner.nextInt();
System.out.println("Prime numbers from 50 to 150 are: ");
	for(num=min;num<=max;num++) {
		count=0;
		for(i=2;i<=num/2;i++) {
			if(num%i==0) {
				count++;
				break;
			}
		}
		if(count==0 && num!=1) {
			System.out.println(num+" ");
		}
	}
	
	
}
}
