package com.app;

import java.util.Collections;
import java.util.LinkedList;

public class LinkedListExample {

	public static void main(String[] args) {
		LinkedList<String> ll=new LinkedList<String>();
		ll.add("Y1");
		ll.add("Y2");
		ll.add("Y3");
		ll.add(null);
		ll.add("UU");
		ll.add("Y3");
		ll.add(null);
		ll.add(1,"srinivas");
		System.out.println(ll);
		Collections.synchronizedCollection(ll);
		System.out.println(ll);
		for (String string : ll) {
			System.out.println(string);
		}
	}

}
