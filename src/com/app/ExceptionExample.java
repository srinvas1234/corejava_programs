package com.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class ExceptionExample {

	public static void main(String[] args) throws FileNotFoundException,IOException {
		System.out.println("program execution start");
		int firstNumber=5;
		int secondNumber=0;
int result=0;

File f=new File("C:\\Users\\srini\\OneDrive\\Documents\\Resume_Update_04_09_2023_Update documents\\Resume_Update_04_09_2023.docx");
FileInputStream fis=new FileInputStream(f);
System.out.println(fis+"file is there");


try {// try block  is mandatory
		result=firstNumber/secondNumber;//risky code
}
catch (Exception e) {//handle exception catch block  is mandatory >> can we use catch or finally must be use while using try block
//System.out.println(e.toString());//write a logic to enter this message into log files
//e.printStackTrace();
//System.out.println(e.getMessage());
//System.out.println(e.getLocalizedMessage());
//throw e;
	
	System.out.println(Arrays.toString(e.getStackTrace()));
	e.printStackTrace();
}finally {//finally block is optional
	System.out.println("I am good");
}
		System.out.println("output:" +result);
		System.out.println("program end");
		
		
	}

}
