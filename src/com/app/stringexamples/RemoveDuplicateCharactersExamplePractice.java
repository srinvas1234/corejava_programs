package com.app.stringexamples;

import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicateCharactersExamplePractice {
public static void main(String[] args) {
	
	String str="java";
	
	
	StringBuilder sb=new StringBuilder();
	str.chars().distinct().forEach(c->sb.append((char)c));
	System.out.println(sb);
	
	StringBuilder sb2=new StringBuilder();
	
	for(int i=0;i<str.length();i++) {
		char ch=str.charAt(i);
		int ii=str.indexOf(ch,i+1);
		if(ii==-1) {
			sb2.append(ch);
		}
	}
	System.out.println(sb2);
	
	
	char[] c=str.toCharArray();
	StringBuilder sb3=new StringBuilder();
	 
	for(int i=0;i<c.length;i++) {
		boolean repeated=false;
		for(int j=i+1;j<c.length;j++) {
			if(c[i]==c[j]) {
			repeated=true;
			break;
		}
	}
	if(!repeated) {
		sb3.append(c[i]);
	}
	}
	System.out.println(sb3);
	
	//Empty StringBuilder class
	StringBuilder sb4=new StringBuilder();
	Set<Character> set=new LinkedHashSet<Character>();
	for(int i=0;i<str.length();i++) {
		set.add(str.charAt(i));
	}
	for (Character character : set) {
		sb4.append(character);
	}
	System.out.println(sb4);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
}
