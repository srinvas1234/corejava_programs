package com.app;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

public class FileHandling {

	public static void main(String[] args) throws IOException {
		//File file=new File("C:\\File_Data\\srinivas.txt");
		//C:\Eclipse\Core_Java_Programs
		File file=new File("C:\\Eclipse\\Core_Java_Programs\\CoreJava_Programs\\src\\com\\app");
		System.out.println(Arrays.toString(file.list()));
		System.out.println(Arrays.toString(file.listFiles()));
		System.out.println(file.getName());//app
		System.out.println(file.getFreeSpace());//392853995520
		System.out.println(file.getPath());//C:\Eclipse\Core_Java_Programs\CoreJava_Programs\src\com\app
		System.out.println(file.getParent());//C:\Eclipse\Core_Java_Programs\CoreJava_Programs\src\com
		System.out.println(file.getAbsoluteFile());//C:\Eclipse\Core_Java_Programs\CoreJava_Programs\src\com\app
		System.out.println(file.getCanonicalFile());//C:\Eclipse\Core_Java_Programs\CoreJava_Programs\src\com\app
		System.out.println(file.toURI());///file:/C:/Eclipse/Core_Java_Programs/CoreJava_Programs/src/com/app/
		System.out.println(file.lastModified());//1695729224155
		System.out.println(new Date(file.lastModified()));//Tue Sep 26 17:23:44 IST 2023
		System.out.println(file.isDirectory());//true
		System.out.println(file.isFile());//false
System.out.println(file.isHidden());//false
System.out.println(file.canWrite());//true
file.setWritable(true);
System.out.println(file.canWrite());//true
//File file=new File("./resume.txt");>>>>current directory [ . ]means
/*
 * boolean b=file.createNewFile(); System.out.println(b);
 */
/*
 * if(file.exists()) { file.delete(); } System.out.println(file.exists());
 * System.out.println(file.createNewFile());
 */
	}

}
