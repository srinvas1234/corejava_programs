package com.app.interviews;

//Java program implementing Singleton class
//with using  getInstance() method
public class Singleton {
	// Static variable reference of single_instance
    // of type Singleton
	private static Singleton single_instance=null;
    // Declaring a variable of type String

	public String s;
	 // Constructor
    // Here we will be creating private constructor
    // restricted to this class itself
	private Singleton() {
		s="Hello Singleton class";
	}
	  // Static method
    // Static method to create instance of Singleton class
	
	public static synchronized Singleton getInstance() {
		if(single_instance==null) {
			single_instance=new Singleton();
		}
		return single_instance;

	}
}
	class Main{
	public static void main(String[] args) {
Singleton x=Singleton.getInstance();
System.out.println(x.hashCode());
Singleton y=Singleton.getInstance();
System.out.println(y.hashCode());
Singleton z=Singleton.getInstance();
System.out.println(z.hashCode());

if(x==y && y==z) {
	System.out.println("Three objects point to the same memory location on the heap i.e, to the same object");
}else {
	System.out.println("Three objects point DONOT to the same memory location on the heap i.e, to the same object");

}

	}

}
