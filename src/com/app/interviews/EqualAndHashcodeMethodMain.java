package com.app.interviews;

public class EqualAndHashcodeMethodMain {
public static void main(String[] args) {
	
	EqualAndHashcodeMethodExamples eqq=new EqualAndHashcodeMethodExamples();
	eqq.setId(333);
	eqq.setName("srinivas");
	System.out.println(eqq.hashCode());
	EqualAndHashcodeMethodExamples eqq1=new EqualAndHashcodeMethodExamples();
	eqq1.setId(333);
	eqq1.setName("srinivas");
	System.out.println(eqq1.hashCode());
	
	
	System.out.println("Shallow Comparision+ "+(eqq==eqq1));
	
	System.out.println("Deep Comparision+ "+(eqq.equals(eqq1)));
	System.out.println(eqq.hashCode());
}
}
