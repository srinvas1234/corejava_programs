package com.app.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Test {

	public static void main(String[] args) {
		/*
		 * Employee e1=new Employee(1, "code",
		 * Arrays.asList("pune","mumbai","hyderbad","Bangalore")); Employee e2=new
		 * Employee(2, "decode", Arrays.asList("abc","bbc","cca","ddb")); Employee
		 * e3=new Employee(3, "wecode", Arrays.asList("ggg","ddd","fff")); Employee
		 * e4=new Employee(4, "youcode", Arrays.asList("you","they","them"));
		 * System.out.println(e1); System.out.println(e2); System.out.println(e3);
		 * System.out.println(e4);
		 */
		
		List<String> citiesWorkedIn=new ArrayList<String>();
		citiesWorkedIn.add("HYD");
		citiesWorkedIn.add("Ban");
		citiesWorkedIn.add("chennai");
		citiesWorkedIn.add("kerala");
		Employee e1=new Employee(1, "code",citiesWorkedIn);
		System.out.println(e1);
		List<String> citiesWorkedIn1=new ArrayList<String>();
		citiesWorkedIn1.add("Ban");
		citiesWorkedIn1.add("chennai");
		citiesWorkedIn1.add("kerala");
		Employee e2=new Employee(2, "decode",citiesWorkedIn1);
		System.out.println(e2);
		
		List<String> citiesWorkedIn2=new ArrayList<String>();
		citiesWorkedIn2.add("Ban");
		citiesWorkedIn2.add("kerala");
		Employee e3=new Employee(3, "youcode",citiesWorkedIn2);
		System.out.println(e3);
		
		List<String> citiesWorkedIn3=new ArrayList<String>();
		citiesWorkedIn3.add("Ban");
		citiesWorkedIn3.add("chennai");
		Employee e4=new Employee(4, "wecode",citiesWorkedIn3);
		System.out.println(e4);
		List<Employee> employeeList=new ArrayList<Employee>();
		employeeList.add(e1);
		employeeList.add(e2);
		employeeList.add(e3);
		employeeList.add(e4);
	System.out.println(employeeList);
		
	/*
	 * List<Integer> ids=new ArrayList<Integer>(); for (Employee employee :
	 * employeeList) { ids.add(employee.getId()); }
	 * 
	 * 
	 * System.out.println(ids);
	 */
	
	
	
	//Using Java8 
	//single value of single input
	//map <<One to one mapping & 
	//it is only mapping & 
	//produces stream of values & 
	//it is only transformation
	
	// number of values as the output.
   //flat map<<One to many mapping &
	//it is mapping and flattering & 
	//produces stream of stream of values & 
	//it is transformation and mapping
	
	
	List<Integer> ss=employeeList.stream().map(emp->emp.getId()).collect(Collectors.toList());
System.out.println(ss);
System.out.println(employeeList);
	
	
Set<String> sss=employeeList.stream().flatMap(emp->emp.getCitiesWorkedIn().stream()).collect(Collectors.toSet());
	System.out.println(sss);
	
	List<Employee> ss11=employeeList.parallelStream().collect(Collectors.toList());
	System.out.println(ss11);
	
	System.out.println("parallel stream");
	employeeList.parallelStream().forEach(System.out::println);
	
	
	System.out.println("parallel stream for each order");
	employeeList.parallelStream().forEachOrdered(System.out::println);
	
	
	System.out.println("stream for each order");
	List<Employee> sw=employeeList.stream().collect(Collectors.toList());
	System.out.println(sw);
	
	
	
	
	
	
	
	
	

	}

}
