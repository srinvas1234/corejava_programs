package com.app;

import java.util.Stack;

public class StackExample {
public static void main(String[] args) {

	
	/* 
	 * 
	 * LIFO last in first out>>>Stack
	 * FIFO  first in first out>>>Queue
	 *
	 *
	 *Stack is a class in java which implements the list interface and extends the vector class and also represents the LIFO principle.
	 *
	 *we can create only Parameterless constructor create
	 * */
	
	
	Stack<String> book=new Stack<String>();
	book.add("RED");
	book.add("GREEN");
	book.add("BLACK");
	book.add("YELLO");
	book.add("WHITE");
	book.add("WHITE");
	book.add(null);
	book.add(null);
	//book.remove(0);
	book.set(1, "orange");

	System.out.println(book.contains("orange"));
	System.out.println(book);
	System.out.println(book.peek());
	System.out.println(book.search("WHITE"));
	System.out.println(book.indexOf("WHITE"));
	System.out.println(book.isEmpty());//isEmpty method coming from Vector
	System.out.println(book.empty());//isEmpty method coming from Stack
	System.out.println(book);
}
}
