package com.app.practicenew;


public class ArrayLargeNumber {

	public static void main(String[] args) {

		int[] a= {4,55,11,1222};
			System.out.println(a.length);
		
		int small=a[0];
		int large=a[0];
		
		for(int i=0;i<a.length;i++) {//0<4
			if(a[i]>large) {
				large=a[i];
			}else if (a[i]<small) {
				small=a[i];
			}
		}
	System.out.println("This is large number: "+large);
	System.out.println("this is small number: "+small);
	 System.out.println("*************");
	
	System.out.println("below normal for loop");
	
	int a1[][]={{1,2,3},{2,4,5},{4,4,5}};
	a1[0][0]=1;
	a1[0][1]=2;
	a1[0][2]=3;
	a1[1][0]=2;
	a1[1][1]=4;
	a1[1][2]=5;
	a1[2][0]=4;
	a1[2][1]=4;
	a1[2][2]=5;
	System.out.println("length: "+ a1.length);
		for (int k = 0; k < a1.length; k++) {  //0<3 true
		for (int k2 = 0; k2 < a1.length; k2++) { //0<3 true
			System.out.print(a1[k][k2]+" ");
		}
		System.out.println();
		
	}
		System.out.println("below for each loop");
	for (int[] is : a1) {
		for (int v : is) {
			System.out.print(v+" ");
		}
		System.out.println();
	}

	}
	
}

