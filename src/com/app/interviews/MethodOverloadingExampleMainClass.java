package com.app.interviews;

public class MethodOverloadingExampleMainClass {
public static void main(String[] args) {
	
	MethodOverloadingExample mm=new MethodOverloadingExample();
	mm.add(1, 2);
	//The method add(int, long) is ambiguous for the type MethodOverloadingExample
	/*
	 * public void add(int a,long b) 
	 * { System.out.println("I am ok: "+a+b); 
	 * } 
	 *public void add(long a,int b)
	 * {
	 *  System.out.println("I am ok: "+a+b); 
	 *  }
	 */
}
}
