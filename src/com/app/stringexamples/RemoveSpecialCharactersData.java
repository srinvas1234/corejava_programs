package com.app.stringexamples;

public class RemoveSpecialCharactersData {

	public static void main(String[] args) {
		
		String str="$ja!va$&st%ar";
		
		//Approach-1(replace All() method
		
		String planString=str.replaceAll("[^a-zA-Z0-9]", "");
		System.out.println(planString);
		

	}

}
