package com.app;

import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;


public class MapExample {

	public static void main(String[] args) {

		//Creation of Map
		Map<Integer,String> m=new Hashtable<Integer, String>();
		//Addition of elements into the map
		m.put(503102, "Jangampalle");
		m.put(503111, "Kamareddy");
		m.put(500038, "HYDERABAD");
		//Retrieval of keys from the map
		Set<Integer> key=m.keySet();
		for (Integer keys : key) {
			System.out.println(keys);
		}
		System.out.println();
		//Retrieval of values from the map
		Collection<String> values=m.values();
		for (String values1 : values) {
			System.out.println(values1);
		}
		System.out.println();
		//Retrieval of values map based on key
		//System.out.println(m.get(503102));
		
		
		for (Integer keys : key) {
			System.out.println(keys+" >>>>"+m.get(keys));
		}		
		System.out.println(m+">>>> Before Deletion");
		//Deletion of elements from the map
		m.remove(500038);
		System.out.println(m+">>>> After Deletion");
		
		//Verification of keys in the map
		
		System.out.println(m.containsKey(503102));//key is  there
		System.out.println(m.containsKey(503103));//key is not there	
	
		//Verification of values in the map
		System.out.println(m.containsValue("Jangampalle"));//key is  there
		System.out.println(m.containsValue("nn"));//key is not there
		//Updation of elements in the map
		System.out.println(m);
		m.put(500038, "Rangareddy");
		m.putIfAbsent(67676, "dddd");
		m.putIfAbsent(67676, "rrrr");
		System.out.println(m);
		System.out.println();
		System.out.println(m);
		m.replace(500038, "SR NAGER");
		System.out.println(m);
		System.out.println(m.size());
//m.clear();	
//System.out.println(m);
		
		Set<Entry<Integer, String>> e=m.entrySet();
		for (Entry<Integer, String> entry : e) {
			System.out.println(entry.getKey()+" -->> "+entry.getValue());
		}
		
		
		
	}

}
