package com.app.interviews;

import java.util.stream.IntStream;

public class StringImmutableExample {
	
	//String -Immuable
	//StringBuffer-Mutable
	//StringBuilder-Mutable
	//String hold only address
	//StringBuffer and StringBuilder hold only data
	
@SuppressWarnings("static-access")
public static void main(String[] args) {
	
	System.out.println("***********StringBuffer**********");
	StringBuffer sb1=new StringBuffer("srinivas");//address-26
	StringBuffer sb2=new StringBuffer("srinivas");//address-36
	System.out.println(sb1==sb2);//26==36
	sb1.append("Vadla");//address-26 srinivasVadla
	System.out.println(sb1==sb2);
	
	StringBuffer sb3=new StringBuffer("Vadlasrinivas");
	System.out.println(sb1==sb3);
	System.out.println(sb1.capacity());
	System.out.println("***********String**********");
	
	String s1="srinivas";
	String s2="srinivas";
	
	System.out.println(s1==s2);
	
	String s3=s1+"Vadla";
	System.out.println(s1==s3);
	
	String s4="srinivasVadla";
	System.out.println(s1==s4);
	System.out.println(s1);
	System.out.println("***********String Builder**********");
	
	StringBuilder sbb1=new StringBuilder("srinivas");
	StringBuilder sbb2=new StringBuilder("srinivas");
	
	char ch=sbb1.charAt(0);
	System.out.println((int)ch);
	System.out.println(ch);
	System.out.println(sbb1.capacity());
	System.out.println(sbb1==sbb2);
	sb1.append("Vadla");//address-26 srinivasVadla
	System.out.println(sbb1==sbb2);
	
	StringBuilder sbb3=new StringBuilder("Vadlasrinivas");
	System.out.println(sbb1==sbb3);
	System.out.println(sb1.capacity());
	
	System.out.println("*********Insert operation in StringBuilder");
	sbb1.insert(0, "Vadla ");
	System.out.println(sbb1);
	
	sbb1.deleteCharAt(4);
	System.out.println(sbb1);
	
	sbb1.delete(0, 5);
	System.out.println(sbb1);
	
	System.out.println(sbb1.reverse());
	System.out.println("*********");
sb1.ensureCapacity(100);
sb1.capacity();
System.out.println(sb1.capacity());

System.out.println("*****************Compare");
System.out.println(s1.equals(s2));
System.out.println(sb1.equals(sb2));
System.out.println(sb1.compareTo(sb2));
System.out.println(sbb1.compareTo(sbb2));

IntStream ii=sbb1.chars();
ii.forEachOrdered(System.out::println);



for (String st : args) {
System.out.println(st.charAt((int)ch));
}




	
}
}
