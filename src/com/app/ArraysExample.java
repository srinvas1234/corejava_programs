package com.app;


public class ArraysExample {
public static void main(String[] args) {
	
	
	int[] p=new int[] {7,63,6,24,6};
	System.out.println(p[0]);	
	System.out.println(p[1]);
	System.out.println(p[2]);
	System.out.println(p[3]);
	System.out.println("length of array : "+p.length);
	System.out.println("***********");
	int[] k=new int[3];
	
	k[0]=3;
	k[1]=5;
	k[2]=6;
	
	System.out.println(k.length);
	System.out.println("**************");
	int[] i= {3,4,1,4,6};
	
	System.out.println(i[0]);
	System.out.println(i[1]);
	System.out.println(i[2]);
	System.out.println(i[3]);
	System.out.println(i[4]);
	
	for (int j = 0; j < i.length; j++) {
		System.out.println(i[j]);
	}
	System.out.println("**********");
	int sum=i[0]+i[1]+i[2]+i[3]+i[4];
	System.out.println(sum);
}
}
