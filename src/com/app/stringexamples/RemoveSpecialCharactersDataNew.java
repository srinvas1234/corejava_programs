package com.app.stringexamples;

public class RemoveSpecialCharactersDataNew {

	public static void main(String[] args) {
		
		String str="&Tit%*($&";
		
		String plain=str.replaceAll("[^a-zA-Z0-9]", "");
		System.out.println(plain);

	}

}
