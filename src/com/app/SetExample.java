package com.app;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SetExample {
public static void main(String[] args) {
	//Hashset implements Set
	//ArrayList internally use Array
	//LinkedList internally multiple Nodes chain process
	//Hashset>>>internally use HashMap
	
	//********* HashSet>>use>>>HashMap>>Array of nodes(key&values)
	//Hash uses whenever we store the data in that it is following hashing mechanism apply after based on Hashcode it will generate index
	//Hashcode specific operation perform one index will be generate	
	HashSet<Integer> set=new HashSet<Integer>();
	//Set<Integer> set1=new HashSet<Integer>();>>it is also fine
	//HashSet>>use>>>HashMap>>Array of nodes(key&values)>>internally IT WILL CREATE one dummy object >>new Object();
	//However in Hashmap we used unique keys so same thing we follow set as well
	//ArrayList>internally>>Arrays
	//LinkedList>internally>>Nodes
	//HashSet<internally>HashMap(keys values)>>Arrays of nodes
	//NO updation operations in SET 
	
	System.out.println(set);
	set.add(344);
	set.add(444);
	set.add(566);
	set.add(444);
	set.add(233);
	set.add(787);
	set.remove(233);
	set.add(null);
	System.out.println(set.remove(233));
	System.out.println(set.contains(787));
	for (Integer set11 : set) {
		System.out.println(set11);
	}
	System.out.println(set);
	System.out.println("*****************");
	//LinkedHashSet>>internally >>LinkedHashMap
	LinkedHashSet<Integer> lhs=new LinkedHashSet<Integer>();
	lhs.add(444);
	lhs.add(544);
	lhs.add(344);
	lhs.add(744);
	lhs.add(844);
	lhs.add(644);
	lhs.add(344);
	lhs.add(944);
	lhs.add(544);
	lhs.add(666);
	for (Integer lset11 : set) {
		System.out.println(lset11);
	}
	System.out.println(lhs);
	
	System.out.println();
	
	System.out.println(lhs.isEmpty());
	System.out.println(lhs.size());
	
	System.out.println("************************");
	
	//Hashset >>>No order followed
	//LinkedHashSet>>>insertion followed
	//TreeSet>>>Sorted ORDER followed
	//TreeSet>>>internally>>TreeMap>>Internally>>>Binary Tree>>Internally use Ascending order
TreeSet<Integer> ts=new TreeSet<Integer>();
ts.add(787);
ts.add(666);
ts.add(544);
ts.add(56);
ts.add(990);
ts.add(877);
ts.add(655);
ts.add(658);
ts.add(655);
System.out.println(ts);
System.out.println();
System.out.println(ts.first());
System.out.println(ts.last());
System.out.println();
System.out.println(ts.floor(990));
System.out.println();
System.out.println(ts.pollFirst());
System.out.println(ts.pollLast());
System.out.println();
System.out.println(ts.subSet(666, 990));
System.out.println();
System.out.println(ts.descendingSet());
System.out.println(ts.tailSet(544));
/*
 * for (Integer tr : ts) { System.out.println(tr); }
 */
	
	System.out.println("***************");
	
	
	
	
	
	
	
	
	
	
	
	HashSet<Integer> set11=new HashSet<Integer>();
	set11.add(344);
	set11.add(444);
	set11.add(566);
	set11.add(444);
	set11.add(233);
	set11.add(787);
	set11.add(787);
	set11.add(null);
	System.out.println(set11);
	
	
	
	
	
	
}
}
