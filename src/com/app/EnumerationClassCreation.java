package com.app;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

public class EnumerationClassCreation {
public static void main(String[] args) {
	//Enumeration-Interface
	//Introduced for iterating the legacy collection objects
	//Legacy classes which is version 1.0 
	//Hashtable ,stack,vector,Directory and properties	
	//New classes which is version 1.2
	//since 1.0 >ALL>>Hashtable ,stack,vector,Directory and properties
	//only forword direction>>> we can't add and delete any eleements like write operations
Vector<String> v=new Vector<String>();
Enumeration<String> e=v.elements();
//e.hasMoreElements()
//e.nextElement()
v.add("banana");
v.add("cherri");
v.add("mango");
v.add("apple");
System.out.println("**********for each");
for (String v1 : v) {
	System.out.println(v1);
}

System.out.println("*********Enumeration");
while (e.hasMoreElements()) {
	System.out.println(e.nextElement());
}
System.out.println("********Using HashTable");
Hashtable<String, Integer> age=new Hashtable<String, Integer>();
age.put("srinivas", 28);
age.put("Nihas", 18);
age.put("sneha", 26);
age.put("siddu", 23);

Enumeration<Integer> ss=age.elements();
while(ss.hasMoreElements()) {
	System.out.println(ss.nextElement());
}

Properties pp=new Properties();
pp.setProperty("url", "localhost");
pp.setProperty("username", "srinivas");
pp.setProperty("password", "srinivas@1234");

Enumeration<Object> sss=pp.elements();
while(sss.hasMoreElements()) {
	System.out.println(sss.nextElement());
}
}
}
