package com.app;

public interface Laptop {

	public void copy();
	public void paste();
	public void cut();
	
	public void keyword();
	
	default void srin() {
		data();
		System.out.println("Hi");
	}
	
	static void main() {
		data();
		System.out.println("how are you");
	}
	
	private static void data() {
		System.out.println("commoncode");
	}
}
