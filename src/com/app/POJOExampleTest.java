package com.app;

public class POJOExampleTest {

	public static void main(String[] args) {

		//POJOExample pp=new POJOExample(333, "srinivas", 787.44);
		POJOExample p=new POJOExample();
		p.setId(7777);
		p.setName("sss");
		p.setSalary(898.99);
		POJOExample p1=new POJOExample();
		p.setId(9777);
		p.setName("sfffs");
		p.setSalary(998.99);
		System.out.println(p);
		System.out.println(p1);
		System.out.println(p.getId());
		System.out.println(p.getName());
		System.out.println(p.getSalary());
		System.out.println(p1.getId());
		System.out.println(p1.getName());
		System.out.println(p1.getSalary());
		
		POJOExample[] pp=new POJOExample[] {p,p1};
		System.out.println(pp[0]);
		System.out.println(pp[1]);
		
	}

}
