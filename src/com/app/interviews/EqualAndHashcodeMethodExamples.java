package com.app.interviews;

public class EqualAndHashcodeMethodExamples {

	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj==null|| getClass()!=obj.getClass()) {
			return false;
		}
		if(obj==this) {
			return true;
		}
		EqualAndHashcodeMethodExamples sss=(EqualAndHashcodeMethodExamples)obj;
		return (this.getId()==sss.getId());
		
	}
	
	@Override
	public int hashCode() {
		
		return getId();
	}
	
	
}
